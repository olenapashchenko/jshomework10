const tabs = document.querySelector(".tabs")
const tabsContent = document.querySelector(".tabs-content")
const tabItems = [...tabsContent.children]

tabs.addEventListener("click", e => {
    const target = e.target
    if (target !== tabs){
        [...tabs.children].forEach(item => {
            item.classList.remove("active")
        })
        tabItems.forEach(item => {
            if (item.dataset.tab === target.dataset.tab){
                item.classList.add("active")
            } else {
                item.classList.remove("active")
            }
        })
        target.classList.toggle("active")
        
    }
    tabItems.forEach(item => {
        if(item.dataset.tab === target.dataset.tab){
            item.classList.add("active")
            item.classList.remove("hidden")
        } else {
            item.classList.add("hidden")
        }
    })
    
})
